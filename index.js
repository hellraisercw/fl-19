const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const { filesRouter } = require('./filesRouter.js');
const path = require('path');

const app = express();
const logStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

app.use(express.json());
app.use(morgan('combined', {stream: logStream}));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    if (!fs.existsSync('password.json')) {
      fs.closeSync(fs.openSync('password.json', 'w'));
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

//ERROR HANDLER
app.use(errorHandler);

function errorHandler (err, req, res, next) {
  console.error(err);
  res.status(500).send({'message': 'Server error'});
}