// requires...

const fs = require('fs');
const path = require('path');

// constants...

const apiPath = './files/';
const extArray = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
let checkPass = false;

// create file

function createFile (req, res, next) {
  const fName = req.body.filename || '';
  const fContent = req.body.content || '';
  const fPwd = req.body.password;
  const existFile = fs.readdirSync(apiPath).find(e => e === fName);
  const ext = extArray.find(e => e === path.extname(fName));
  if (!fName) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });
  } else if (!ext) {
    res.status(400).send({ "message": `Please choose one of the supported extensions: '${extArray.join("', '")}'` });
  } else if (!fContent) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });
  } else if (existFile) {
    res.status(400).send({ "message": "File already exists" });
  } else {
    if (fPwd) {
      createPassword(fName, fPwd);
    }
    fs.writeFile(apiPath + fName, fContent, err => {
      if (err) throw err;
    });
    res.status(200).send({ "message": "File created successfully" });
  }
  next();
}

// get all files

function getFiles (req, res, next) {
  res.status(200).send({
    "message": "Success",
    "files": fs.readdirSync(apiPath)
  });
  next();
}

// read file

function getFile (req, res, next) {
  const fName = req.params.filename;
  const existFile = fs.readdirSync(apiPath).find(e => e === fName);
  if (!existFile) {
    res.status(400).send({ "message": `No file with '${fName}' filename found` });
  } else {
    const queryPass = req.query.password;
    const pwdFile = fs.readFileSync("password.json");
    if (pwdFile.length > 0) {
      checkPass = checkPassword(pwdFile, fName, queryPass, res);
    }
    if (pwdFile.length === 0 || checkPass === true) {
      res.status(200).send({
        "message": "Success",
        "filename": fName,
        "content": fs.readFileSync(apiPath + fName, 'utf8'),
        "extension": path.extname(fName).slice(1),
        "uploadedDate": fs.statSync(apiPath + fName).birthtime
      });
    }
  }
  next();
}

// edit file

function editFile (req, res, next) {
  const fName = req.body.filename;
  const fContent = req.body.content;
  const queryPass = req.body.password;
  const existFile = fs.readdirSync(apiPath).find(e => e === fName);
  const pwdFile = fs.readFileSync("password.json");
  if (!fName) {
    res.status(400).send({ "message": "Please specify file to modify" });
  } else if (!existFile) {
    res.status(400).send({ "message": `No file with '${fName}' filename found` });
  } else {
    if (pwdFile.length > 0) {
      checkPass = checkPassword(pwdFile, fName, queryPass, res);
    }
    if (pwdFile.length === 0 || checkPass === true) {
      fs.appendFileSync(apiPath + fName, fContent, err => {
        if (err) throw err;
      });
      res.status(200).send({ "message": `File with '${fName}' filename successfully edited` });
    }
  }
  next();
}

// delete file

function deleteFile (req, res, next) {
  const fName = req.params.filename;
  const queryPass = req.query.password;
  const existFile = fs.readdirSync(apiPath).find(e => e === fName);
  const pwdFile = fs.readFileSync("password.json");

  if (!existFile) {
    res.status(400).send({ "message": `No file with '${fName}' filename found` });
  } else {
    if (pwdFile.length > 0) {
      checkPass = checkPassword(pwdFile, fName, queryPass, res);
    }
    if (pwdFile.length === 0 || checkPass === true) {
      fs.unlink(apiPath + fName, (err) => {
        if (err) throw err;
      });
      deletePassword (pwdFile, fName);
      res.status(200).send({ "message": `File with '${fName}' filename successfully deleted` });
    }
  }
  next();
}

// save password

function createPassword (fName, fPwd) {
  const pwdFile = fs.readFileSync("password.json");
  const newPass = {"filename": fName, "password": fPwd};
  if (pwdFile.length === 0) {
    fs.writeFileSync("password.json", JSON.stringify([newPass]), err => {
      if (err) throw err;
    });
  } else {
    const jsonPass = JSON.parse(pwdFile.toString());
    const isPass = jsonPass.find(i => i.filename === fName);
    if (isPass) {
      isPass.password = fPwd;
    } else {
      jsonPass.push(newPass);
    }
    fs.writeFileSync("password.json", JSON.stringify(jsonPass), err => {
      if (err) throw err;
    });
  }
}

// check password

function checkPassword (pwdFile, fName, queryPass, res) {
  const jsonPass = JSON.parse(pwdFile.toString());
  const isPass = jsonPass.find(e => e.filename === fName);
  if (isPass) {
    if (!queryPass) {
      return res.status(400).send({ "message": "Please enter password" });
    } else if (isPass.password !== queryPass) {
      return res.status(400).send({ "message": "Wrong password" });
    } else {
      return true;
    }
  } else {
    return true;
  }
}

// delete password

function deletePassword (pwdFile, fName) {
  const jsonPass = JSON.parse(pwdFile.toString());
  const index = jsonPass.findIndex(e => e.filename === fName);
  jsonPass.splice(index, 1);
  fs.writeFileSync("password.json", JSON.stringify(jsonPass), err => {
    if (err) throw err;
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
};